var express = require('express');

var app = express();
var bodyParser = require('body-parser');
var multer = require('multer'); 

app.use(bodyParser.json()); // for parsing application/json
app.use(bodyParser.urlencoded({ extended: true })); // for parsing application/x-www-form-urlencoded
app.use(multer()); // for parsing multipart/form-data

app.get('/', function (req, res){
	res.send('OK');
});


app.post('/geolocation', function (req, res){
	res.json(['body OK:', req.body, 'params OK:', req.params, 'query OK:',req.query]);
	console.log("body: " + JSON.stringify(req.body) + " params: " + JSON.stringify(req.params) + " query: " + JSON.stringify(req.query));
});

var server =  app.listen(3000, function(){
	console.log('Listening on .. http://%s:%s', server.address().address, server.address().port);
});
